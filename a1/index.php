<?php require_once './code.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04: Access Modifier and Inheritance</title>
</head>
<body>

	<h1>Access Modifiers</h1>

	<h2>Building</h2>

	<p><?php //echo $building->name; ?></p>

	<h2>Condominium Variables</h2>

	<p><?php //echo $condominium->name; ?></p>

	<h2>Encapsultion</h2>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

	<p><?php $condominium->setName('Enzo Tower'); ?></p>

	<p>The name of the condominium has change to <?php echo $condominium->getName(); ?></p>


	<h1>Activity solution</h1>

	<h2>Building</h2>

	<p>The name of the building is <?php echo $building->getName(); ?></p>
	<p>The <?php echo $building->getName();?> has <?php echo $building->getFloors();?> floors</p>

	<p>The <?php echo $building->getName();?> is located at <?php echo $building->getAddress();?></p>

	<p><?php $building->setName('Caswynn Complex'); ?></p>

	<p>The name of the building has changed to <?php echo $building->getName(); ?></p>


	<h2>Condominium</h2>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
	<p>The <?php echo $condominium->getName();?> has <?php echo $condominium->getFloors();?> floors</p>

	<p>The <?php echo $condominium->getName();?> is located at <?php echo $condominium->getAddress();?></p>

	<p><?php $condominium->setName('Enzo Tower'); ?></p>

	<p>The name of the condominium has changed to <?php echo $condominium->getName(); ?></p>

</body>
</html>