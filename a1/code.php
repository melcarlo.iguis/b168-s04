<?php


class Building {
	protected $name;
	protected $floors;
	protected $address;

	// Contructor is used during the creation of an object.
	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	// Encapsulation - getter
	public function getName(){
		return $this->name;
	}

	// Encapsulation - setter
	public function setName($name){
		$this->name = $name;
	}

	// Encapsulation - getter
	public function getFloors(){
		return $this->floors;
	}

	// Encapsulation - setter
	public function setFloor($floors){
		$this->floors = $floors;
	}


	// Encapsulation - getter
	public function getAddress(){
		return $this->address;
	}

	// Encapsulation - setter
	public function setAddress($address){
		$this->address = $address;
	}
}

class Condominium extends Building{
	// Encapsulation - getter
	public function getName(){
		return $this->name;
	}

	// Encapsulation - setter
	public function setName($name){
		$this->name = $name;
	}

	// Encapsulation - getter
	public function getFloors(){
		return $this->floors;
	}

	// Encapsulation - setter
	public function setFloor($floors){
		$this->floors = $floors;
	}


	// Encapsulation - getter
	public function getAddress(){
		return $this->address;
	}

	// Encapsulation - setter
	public function setAddress($address){
		$this->address = $address;
	}
}


$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo' , '5' , 'Buendia Ave, Makati City, Philippines');